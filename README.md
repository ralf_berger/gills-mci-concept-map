# Concept Map zu Methoden und Modellen der MCI

Dieser Graph ist eine Concept Map von Methoden und Modellen der MCI für die Veranstaltung "Gestaltung interaktiver Lehr-/Lernsysteme" im Sommersemester 2013.

* [Aufgabenstellung](http://moodle.collide.info/mod/assign/view.php?id=5401)


## Aktuelle Version

![Concept map als Grafik](https://bitbucket.org/ralf_berger/gills-mci-concept-map/downloads/mci.png "Concept Map zu Methoden und Modellen der MCI")


## Anforderungen

* Graphviz


## Erzeugen des Graphen

```bash
dot -Tpdf mci.dot > mci.pdf
```

Für JPEG, PNG, SVG oder andere Formate entsprechende Parameter verwenden.
